"use strict";
// DATA SECTION
const TeamObject = {
  recordLink() {
    return `https://records.nhl.com/awards/trophies/winner/stanley-cup?season=${this.seasonStart}${this.seasonEnd}`;
  },
  teamLogoPath() {
    return `images/${this.teamLogo}_dark.svg`;
  },
  season() {
    return `${this.seasonStart}-${String(this.seasonEnd).substring(2, 4)}`;
  },
  calcMinMax() {
    const l = this.cupLeftEdge;
    const r = this.cupRightEdge;
    const minMax = [l, r];
    if (l < r) {
      minMax[0] = l + 359;
      minMax[1] = r;
    } else {
      minMax[0] = l;
      minMax[1] = r;
    }
    return minMax;
  },
  init(seasonEnd, teamCity, teamName, teamLogo, cupLeftEdge, cupRightEdge) {
    this.seasonEnd = seasonEnd;
    this.seasonStart = seasonEnd - 1;
    this.teamCity = teamCity;
    this.teamName = teamName;
    this.teamLogo = teamLogo;
    this.cupLeftEdge = cupLeftEdge;
    this.cupRightEdge = cupRightEdge;
    this.getSeason = this.season();
    this.getTeamLogoPath = this.teamLogoPath();
    this.getRecordLink = this.recordLink();
  },
};
const teamCities = {
  montreal: "Montréal",
  montrealm: "Montreal",
  toronto: "Toronto",
  boston: "Boston",
  philadelphia: "Philadelphia",
  newYork: "New York",
  edmonton: "Edmonton",
  calgary: "Calgary",
  pittsburgh: "Pittsburgh",
  newJersey: "New Jersey",
  colorado: "Colorado",
  detroit: "Detroit",
  dallas: "Dallas",
  tampaBay: "Tampa Bay",
  carolina: "Carolina",
  anaheim: "Anaheim",
  chicago: "Chicago",
  losAngeles: "Los Angeles",
  washington: "Washington",
  stlouis: "St. Louis",
  ottawa: "Ottawa",
  stanleyCup: "Stanley Cup",
  nhl: "NHL",
};
const teamNames = {
  canadiens: "Canadiens",
  mapleLeafs: "Maple Leafs",
  bruins: "Bruins",
  flyers: "Flyers",
  islanders: "Islanders",
  oilers: "Oilers",
  flames: "Flames",
  penguins: "Penguins",
  rangers: "Rangers",
  devils: "Devils",
  avalanche: "Avalanche",
  redWings: "Red Wings",
  stars: "Stars",
  lightning: "Lightning",
  hurricanes: "Hurricanes",
  ducks: "Ducks",
  blackhawks: "Blackhawks",
  kings: "Kings",
  capitals: "Capitals",
  blues: "Blues",
  senators: "Senators",
  maroons: "Maroons",
  history: "History",
  legacy: "Legacy",
};
const teamLogos = {
  mmr34: "MMR_19251926-19341935",
  bos31: "BOS_19261927-19311932",
  sen33: "SEN_19171918-19331934",
  mtl24: "MTL_19221923-19241925",
  mtl98: "MTL_19561957-19981999",
  tor66: "TOR_19631964-19661967",
  bos94: "BOS_19491950-19941995",
  phi98: "PHI_19671968-19981999",
  nyi94: "NYI_19721973-19941995",
  edm85: "EDM_19791980-19851986",
  edm95: "EDM_19861987-19951996",
  cgy93: "CGY_19801981-19931994",
  pit91: "PIT_19721973-19911992",
  nyr98: "NYR_19781979-19981999",
  njd98: "NJD_19921993-19981999",
  col98: "COL_19951996-19981999",
  det: "DET",
  dal12: "DAL_19941995-20122013",
  njd: "NJD",
  col: "COL",
  tbl06: "TBL_20012002-20062007",
  car12: "CAR_19992000-20122013",
  ana12: "ANA_20062007-20122013",
  pit15: "PIT_20062007-20152016",
  chi: "CHI",
  bos: "BOS",
  lak18: "LAK_20112012-20182019",
  pit: "PIT",
  wsh: "WSH",
  stl: "STL",
  tbl: "TBL",
  cup92: "CUP_1892",
  nhl46: "NHL_19171918-19461947",
};
const cup1892 = Object.create(TeamObject);
cup1892.init(1893, teamCities.stanleyCup, teamNames.history, teamLogos.cup92);
const mmr1925 = Object.create(TeamObject);
mmr1925.init(1926, teamCities.montrealm, teamNames.maroons, teamLogos.mmr34);

const nhl1946 = Object.create(TeamObject);
nhl1946.init(1918, teamCities.nhl, teamNames.legacy, teamLogos.nhl46);

const bos1928 = Object.create(TeamObject);
const sen1926 = Object.create(TeamObject);
bos1928.init(1929, teamCities.boston, teamNames.bruins, teamLogos.bos31);
sen1926.init(1927, teamCities.ottawa, teamNames.senators, teamLogos.sen33);
// 1924 Band
const mtl1923 = Object.create(TeamObject);
mtl1923.init(1924, teamCities.montreal, teamNames.canadiens, teamLogos.mtl24);
// ROW 1
const mtl1965 = Object.create(TeamObject);
const tor1966 = Object.create(TeamObject);
const mtl1967 = Object.create(TeamObject);
const mtl1968 = Object.create(TeamObject);
const bos1969 = Object.create(TeamObject);
const mtl1970 = Object.create(TeamObject);
const bos1971 = Object.create(TeamObject);
const mtl1972 = Object.create(TeamObject);
const phi1973 = Object.create(TeamObject);
const phi1974 = Object.create(TeamObject);
const mtl1975 = Object.create(TeamObject);
const mtl1976 = Object.create(TeamObject);
const mtl1977 = Object.create(TeamObject);
mtl1965.init(1966, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
tor1966.init(1967, teamCities.toronto, teamNames.mapleLeafs, teamLogos.tor66);
mtl1967.init(1968, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
mtl1968.init(1969, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
bos1969.init(1970, teamCities.boston, teamNames.bruins, teamLogos.bos94);
mtl1970.init(1971, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
bos1971.init(1972, teamCities.boston, teamNames.bruins, teamLogos.bos94);
mtl1972.init(1973, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
phi1973.init(1974, teamCities.philadelphia, teamNames.flyers, teamLogos.phi98);
phi1974.init(1975, teamCities.philadelphia, teamNames.flyers, teamLogos.phi98);
mtl1975.init(1976, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
mtl1976.init(1977, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
mtl1977.init(1978, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
// ROW 2
const mtl1978 = Object.create(TeamObject);
const nyi1979 = Object.create(TeamObject);
const nyi1980 = Object.create(TeamObject);
const nyi1981 = Object.create(TeamObject);
const nyi1982 = Object.create(TeamObject);
const edm1983 = Object.create(TeamObject);
const edm1984 = Object.create(TeamObject);
const mtl1985 = Object.create(TeamObject);
const edm1986 = Object.create(TeamObject);
const edm1987 = Object.create(TeamObject);
const cgy1988 = Object.create(TeamObject);
const edm1989 = Object.create(TeamObject);
const pit1990 = Object.create(TeamObject);
mtl1978.init(1979, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
nyi1979.init(1980, teamCities.newYork, teamNames.islanders, teamLogos.nyi94);
nyi1980.init(1981, teamCities.newYork, teamNames.islanders, teamLogos.nyi94);
nyi1981.init(1982, teamCities.newYork, teamNames.islanders, teamLogos.nyi94);
nyi1982.init(1983, teamCities.newYork, teamNames.islanders, teamLogos.nyi94);
edm1983.init(1984, teamCities.edmonton, teamNames.oilers, teamLogos.edm85);
edm1984.init(1985, teamCities.edmonton, teamNames.oilers, teamLogos.edm85);
mtl1985.init(1986, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
edm1986.init(1987, teamCities.edmonton, teamNames.oilers, teamLogos.edm95);
edm1987.init(1988, teamCities.edmonton, teamNames.oilers, teamLogos.edm95);
cgy1988.init(1989, teamCities.calgary, teamNames.flames, teamLogos.cgy93);
edm1989.init(1990, teamCities.edmonton, teamNames.oilers, teamLogos.edm95);
pit1990.init(1991, teamCities.pittsburgh, teamNames.penguins, teamLogos.pit91);
// ROW 3
const pit1991 = Object.create(TeamObject);
const mtl1992 = Object.create(TeamObject);
const nyr1993 = Object.create(TeamObject);
const njd1994 = Object.create(TeamObject);
const col1995 = Object.create(TeamObject);
const det1996 = Object.create(TeamObject);
const det1997 = Object.create(TeamObject);
const dal1998 = Object.create(TeamObject);
const njd1999 = Object.create(TeamObject);
const col2000 = Object.create(TeamObject);
const det2001 = Object.create(TeamObject);
const njd2002 = Object.create(TeamObject);
const tbl2003 = Object.create(TeamObject);
pit1991.init(1992, teamCities.pittsburgh, teamNames.penguins, teamLogos.pit91);
mtl1992.init(1993, teamCities.montreal, teamNames.canadiens, teamLogos.mtl98);
nyr1993.init(1994, teamCities.newYork, teamNames.rangers, teamLogos.nyr98);
njd1994.init(1995, teamCities.newJersey, teamNames.devils, teamLogos.njd98);
col1995.init(1996, teamCities.colorado, teamNames.avalanche, teamLogos.col98);
det1996.init(1997, teamCities.detroit, teamNames.redWings, teamLogos.det);
det1997.init(1998, teamCities.detroit, teamNames.redWings, teamLogos.det);
dal1998.init(1999, teamCities.dallas, teamNames.stars, teamLogos.dal12);
njd1999.init(2000, teamCities.newJersey, teamNames.devils, teamLogos.njd);
col2000.init(2001, teamCities.colorado, teamNames.avalanche, teamLogos.col);
det2001.init(2002, teamCities.detroit, teamNames.redWings, teamLogos.det);
njd2002.init(2003, teamCities.newJersey, teamNames.devils, teamLogos.njd);
tbl2003.init(2004, teamCities.tampaBay, teamNames.lightning, teamLogos.tbl06);

// ROW 4
// Empty Space
const car2005 = Object.create(TeamObject);
const ana2006 = Object.create(TeamObject);
const det2007 = Object.create(TeamObject);
const pit2008 = Object.create(TeamObject);
const chi2009 = Object.create(TeamObject);
const bos2010 = Object.create(TeamObject);
const lak2011 = Object.create(TeamObject);
const chi2012 = Object.create(TeamObject);
const lak2013 = Object.create(TeamObject);
const chi2014 = Object.create(TeamObject);
const pit2015 = Object.create(TeamObject);
const pit2016 = Object.create(TeamObject);
car2005.init(2006, teamCities.carolina, teamNames.hurricanes, teamLogos.car12);
ana2006.init(2007, teamCities.anaheim, teamNames.ducks, teamLogos.ana12);
det2007.init(2008, teamCities.detroit, teamNames.redWings, teamLogos.det);
pit2008.init(2009, teamCities.pittsburgh, teamNames.penguins, teamLogos.pit15);
chi2009.init(2010, teamCities.chicago, teamNames.blackhawks, teamLogos.chi);
bos2010.init(2011, teamCities.boston, teamNames.bruins, teamLogos.bos);
lak2011.init(2012, teamCities.losAngeles, teamNames.kings, teamLogos.lak18);
chi2012.init(2013, teamCities.chicago, teamNames.blackhawks, teamLogos.chi);
lak2013.init(2014, teamCities.losAngeles, teamNames.kings, teamLogos.lak18);
chi2014.init(2015, teamCities.chicago, teamNames.blackhawks, teamLogos.chi);
pit2015.init(2016, teamCities.pittsburgh, teamNames.penguins, teamLogos.pit15);
pit2016.init(2017, teamCities.pittsburgh, teamNames.penguins, teamLogos.pit);
// ROW 5
const wsh2017 = Object.create(TeamObject);
const stl2018 = Object.create(TeamObject);
const tbl2019 = Object.create(TeamObject);
const tbl2020 = Object.create(TeamObject);
wsh2017.init(2018, teamCities.washington, teamNames.capitals, teamLogos.wsh);
stl2018.init(2019, teamCities.stlouis, teamNames.blues, teamLogos.stl);
tbl2019.init(2020, teamCities.tampaBay, teamNames.lightning, teamLogos.tbl);
tbl2020.init(2021, teamCities.tampaBay, teamNames.lightning, teamLogos.tbl);

const edges = [
  [360, 0],
  [360, 0],
  [360, 0],
  [360, 0],
  [360, 0],
  [360, 0],
  [360, 0],
  [
    283.6, 256.2, 228.4, 200.8, 172.6, 145.8, 118.4, 90.4, 62.4, 35.2, 7.4,
    339.6, 311.8, 283.6,
  ],
  [
    250.8, 223, 196, 168, 140.2, 112.8, 85.4, 57.2, 30, 2, 334.2, 306.4, 278.8,
    250.8,
  ],
  [
    276.2, 248.6, 221.1, 193.3, 165.1, 137.6, 110.5, 83, 54.9, 27.6, 0, 332,
    303.6, 276.2,
  ],
  [
    280, 252.6, 224.4, 196.6, 168.6, 141.2, 113.4, 86, 58.4, 30.6, 2.8, 335.2,
    307.2,
  ],
  [335.2, 307.8, 279.8, 252.6, 224.2],
];
//  [sen1926, bos1928],
const rows = [
  [cup1892],
  [mmr1925],
  [nhl1946],
  [sen1926],
  [nhl1946],
  [mtl1923],
  [nhl1946],
  [
    mtl1965,
    tor1966,
    mtl1967,
    mtl1968,
    bos1969,
    mtl1970,
    bos1971,
    mtl1972,
    phi1973,
    phi1974,
    mtl1975,
    mtl1976,
    mtl1977,
  ],
  [
    mtl1978,
    nyi1979,
    nyi1980,
    nyi1981,
    nyi1982,
    edm1983,
    edm1984,
    mtl1985,
    edm1986,
    edm1987,
    cgy1988,
    edm1989,
    pit1990,
  ],
  [
    pit1991,
    mtl1992,
    nyr1993,
    njd1994,
    col1995,
    det1996,
    det1997,
    dal1998,
    njd1999,
    col2000,
    det2001,
    njd2002,
    tbl2003,
  ],
  [
    car2005,
    ana2006,
    det2007,
    pit2008,
    chi2009,
    bos2010,
    lak2011,
    chi2012,
    lak2013,
    chi2014,
    pit2015,
    pit2016,
  ],
  [wsh2017, stl2018, tbl2019, tbl2020],
];
for (let i = 0; i < rows.length; i++) {
  for (let j = 0; j < rows[i].length; j++) {
    rows[i][j].cupLeftEdge = edges[i][j];
    rows[i][j].cupRightEdge = edges[i][j + 1];
    rows[i][j].left = rows[i][j].calcMinMax()[0];
    rows[i][j].right = rows[i][j].calcMinMax()[1];
  }
}
// Legacy, MTL, Legacy, OTT/BOS, Legacy, MAR, CUP
const heights = [
  1.0, 0.7, 0.671, 0.655, 0.614, 0.59, 0.537, 0.453, 0.377, 0.29, 0.2, 0.115,
  0.035,
];

// VAR SECTION
// const arDebug = document.querySelector(".ar-debug");
// const arRot = document.querySelector(".ar-rot");
// const arPan = document.querySelector(".ar-pan");

const mv = document.querySelector("model-viewer");
const startAR = document.querySelector(".start-ar");
const arLogo = document.querySelector(".ar-logo");
const pointerAR = document.querySelector(".nhl-ar-pointer");
const loadingAR = document.querySelector(".ar-loading");
const loadingTextAR = document.querySelector(".ar-loading-text");

const arMenu = document.querySelector(".ar-menu");
const seasonAR = document.querySelector(".season-ar");
const teamLogoAR = document.querySelector(".team-logo-ar");
const nameCityAR = document.querySelector(".name-city-ar");
const nameTeamAR = document.querySelector(".name-team-ar");

const arPopUp = document.querySelector(".ar-popup");
const arPopUpHide = document.querySelector(".ar-popup-hide");
const arPopUpRecords = document.querySelector(".records-link");

const gesturePath = "images/";
const gestureExt = ".svg";
const gestureImage = {
  pan: "gestures-pan",
  rotate: "gestures-rotate",
  zoom: "gestures-zoom",
  click: "gestures-click",
};
const gestureAnimation = {
  pan: "panGesture",
  rotate: "rotateGesture",
  zoom: "zoomGesture",
  click: "clickGesture",
};
const arGestures = document.querySelector(".ar-gestures");
const arGestureImage = document.querySelector(".ar-gesture-image");
let canGesture = true;
let currentGesture = "pan";

const getMobileOS = () => {
  const ua = navigator.userAgent;
  if (/android/i.test(ua)) {
    return "Android";
  } else if (
    /iPad|iPhone|iPod/.test(ua) ||
    (navigator.platform === "MacIntel" && navigator.maxTouchPoints > 1)
  ) {
    return "iOS";
  }
  return "Other";
};
const os = getMobileOS();

function changeImage(image, animation) {
  arGestureImage.setAttribute("src", `${gesturePath}${image}${gestureExt}`);
  arGestureImage.style.setProperty("animation-name", animation);
}
function gestures(move) {
  if (canGesture === false) return;
  if (move === currentGesture) {
    if (move === "pan") {
      currentGesture = "rotate";
      changeImage(gestureImage.rotate, gestureAnimation.rotate);
    } else if (move === "rotate") {
      currentGesture = "zoom";
      changeImage(gestureImage.zoom, gestureAnimation.zoom);
    } else if (move === "zoom") {
      currentGesture = "click";
      changeImage(gestureImage.click, gestureAnimation.click);
      arGestures.style.setProperty("top", "40px");
      arGestures.style.setProperty("left", "calc(50vw + 50px)");
      arGestures.style.setProperty("z-index", "3000");
    }
  }
}

//console.log(mv.canActivateAR);
mv.minFieldOfView = "0deg";
mv.maxFieldOfView = "180deg";
mv.fieldOfView = "10deg";
let currentLink;
let panY, rotY;
let inputX, inputY;
let zoomPercent = 1.0;
let panYStart = 0.5;
let rotYStart = 0;
let isDownTouch = false;
let startZoom = 0;
let currentZoom = 0;
let distance = 0;
const zoomSpeedReduction = 10000;
// LOAD TEXTURES SECTION
let texturePath = "textures/";
const textureExt = ".jpg";
const textureNorm = {
  c1n1: "c1n1",
  c2n1: "c2n1",
  c4n1: "c4n1",
  c5n1: "c5n1",
  c6n1: "c6n1",
  r1n1: "r1n1",
  r2n1: "r2n1",
  r3n1: "r3n1",
  r4n1: "r4n1",
  r5n1: "r5n1",
  r6n1: "r6n1",
};

function heightFix() {
  document
    .querySelector(":root")
    .style.setProperty("--vh", window.innerHeight / 100 + "px");
  document
    .querySelector(":root")
    .style.setProperty("--hw", window.innerWidth / 100 + "px");
}
window.addEventListener("resize", (e) => {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    heightFix();
  }
});
window.addEventListener("load", (e) => {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    heightFix();
  }
});

// function arDebugging() {
//   arRot.textContent = "Rotation: " + rotY + ", ";
//   arPan.textContent = "Pan: " + panY;
// }
let isLoaded = false;
mv.addEventListener("load", () => {
  if (mv.getAttribute("ar-status") === "not-presenting" && isLoaded === false) {
    if (os === "Other") texturePath = "textures4k/";
    let texturesLoaded = 0;
    const materials = mv.model.materials;
    console.log(materials);
    const material = {
      c1m1: materials[10],
      c2m1: materials[9],
      c4m1: materials[1],
      c5m1: materials[0],
      c6m1: materials[2],
      r1m1: materials[3],
      r2m1: materials[5],
      r3m1: materials[6],
      r4m1: materials[7],
      r5m1: materials[8],
      r6m1: materials[4],
    };

    const createAndApplyDiffuseTexture = async (mat, name) => {
      loadingTextAR.textContent = `Loading.. ${texturesLoaded}/11`;
      const texture = await mv.createTexture(
        `${texturePath}${name}${textureExt}`
      );
      texture.name = name;
      mat.pbrMetallicRoughness["baseColorTexture"].setTexture(texture);
      if (isLoaded === false) {
        texturesLoaded++;
        isLoaded = true;
        cupLoaded();
      }
    };
    const createAndApplyNormalTexture = async (mat, name) => {
      loadingTextAR.textContent = `Loading.. ${texturesLoaded}/11`;
      const texture = await mv.createTexture(
        `${texturePath}${name}${textureExt}`
      );
      texture.name = name;
      mat["normalTexture"].setTexture(texture);
      texturesLoaded++;
      if (texturesLoaded === 1 && isLoaded === false) {
        createAndApplyNormalTexture(material.c2m1, textureNorm.c2n1);
      } else if (texturesLoaded === 2 && isLoaded === false) {
        createAndApplyNormalTexture(material.c4m1, textureNorm.c4n1);
      } else if (texturesLoaded === 3 && isLoaded === false) {
        createAndApplyNormalTexture(material.c5m1, textureNorm.c5n1);
      } else if (texturesLoaded === 4 && isLoaded === false) {
        createAndApplyNormalTexture(material.c6m1, textureNorm.c6n1);
      } else if (texturesLoaded === 5 && isLoaded === false) {
        createAndApplyNormalTexture(material.r1m1, textureNorm.r1n1);
      } else if (texturesLoaded === 6 && isLoaded === false) {
        createAndApplyNormalTexture(material.r2m1, textureNorm.r2n1);
      } else if (texturesLoaded === 7 && isLoaded === false) {
        createAndApplyNormalTexture(material.r3m1, textureNorm.r3n1);
      } else if (texturesLoaded === 8 && isLoaded === false) {
        createAndApplyNormalTexture(material.r4m1, textureNorm.r4n1);
      } else if (texturesLoaded === 9 && isLoaded === false) {
        createAndApplyNormalTexture(material.r5m1, textureNorm.r5n1);
      } else if (texturesLoaded === 10 && isLoaded === false) {
        createAndApplyNormalTexture(material.r6m1, textureNorm.r6n1);
      } else if (texturesLoaded === 11 && isLoaded === false) {
        createAndApplyDiffuseTexture(materials[11], "c3a1");
      }
    };
    createAndApplyNormalTexture(material.c1m1, textureNorm.c1n1);
  }
});
pointerAR.style.opacity = 0;

let areListenersAdded = false;

function arMenuClick(e) {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    if (canGesture && currentGesture === "click") {
      canGesture = false;
      arGestures.classList.remove("ar-getures-show");
      arGestures.classList.add("ar-hide");
      arMenu.classList.remove("ar-hide");
    }
    const wait = zoomPercent;
    zoomPercent = 0;
    mv.cameraOrbit = `${-rotY}deg 90deg ${zoomPercent * 100}%`;
    setTimeout(function () {
      arPopUp.classList.add("ar-popup-show");
    }, 500 * wait);
  }
}
function arPopHideClick(e) {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    arPopUp.classList.remove("ar-popup-show");
  }
}
function arPopUpRecordsClick(e) {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    window.open(currentLink, "_blank");
  }
}

// MOBILE AR

function startARClick(e) {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    if (os !== "iOS") {
      arPopUp.classList.remove("ar-popup-show");
      arGestures.classList.remove("ar-getures-show");
      arGestures.classList.add("ar-hide");
      startAR.classList.add("ar-hide");
    }

    let wait = zoomPercent;
    zoomPercent = 0;
    mv.cameraOrbit = `${-rotY}deg 90deg ${zoomPercent * 100}%`;
    setTimeout(function () {
      mv.activateAR();
    }, 500 * wait);
  }
}
function mvTouchStart(e) {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    if (e.touches.length > 1) {
      startZooming(e.touches[0].pageX, e.touches[1].pageX);
    } else {
      startMoving(e.touches[0].pageY, e.touches[0].pageX);
    }
  }
}
function mvTouchMove(e) {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    e.preventDefault();
    if (e.touches.length > 1) {
      zooming(e.touches[0].pageX, e.touches[1].pageX);
    } else {
      startZoom = 0;
      currentZoom = 0;
      distance = 0;
      moving(e.touches[0].pageY, e.touches[0].pageX);
    }
  }
}
function mvTouchEnd(e) {
  if (mv.getAttribute("ar-status") === "not-presenting") {
    endMoving();
  }
}
function mvARStatus(event) {
  if (os !== "iOS") {
    if (event.detail.status === "not-presenting" && mv.canActivateAR) {
      startAR.classList.remove("ar-hide");
    }
  }
}

// DESKTOP
function mvMouseDown(e) {
  startMoving(e.offsetY, e.offsetX);
}
function mvMouseMove(e) {
  moving(e.offsetY, e.offsetX);
}
function mvMouseUp(e) {
  endMoving();
}
function mvWheel(e) {
  e.preventDefault();
  let zoomAmount = e.deltaY * 0.001;
  zoomPercent = zoomPercent + zoomAmount;
  if (zoomPercent < 0.0) zoomPercent = 0.0;
  if (zoomPercent > 1.0) zoomPercent = 1.0;
  pointerAR.style.opacity = 0.2 + zoomPercent;
  pointerAR.style.width = `${5 + 35 * zoomPercent}px`;
  pointerAR.style.height = `${5 + 35 * zoomPercent}px`;
  mv.cameraOrbit = `${-rotY}deg 90deg ${zoomPercent * 100}%`;
  gestures("zoom");
}

function cupLoaded() {
  if (
    mv.getAttribute("ar-status") === "not-presenting" &&
    areListenersAdded === false
  ) {
    mv.dismissPoster();
    pointerAR.style.opacity = 1;
    loadingAR.style.opacity = 0;
    loadingAR.style.display = "none";

    arMenu.addEventListener("click", arMenuClick, true);
    arPopUpHide.addEventListener("click", arPopHideClick, true);
    arPopUpRecords.addEventListener("click", arPopUpRecordsClick), true;

    if (mv.canActivateAR) {
      startAR.addEventListener("click", startARClick), true;
      mv.addEventListener("touchstart", mvTouchStart, true);
      mv.addEventListener("touchmove", mvTouchMove, true);
      mv.addEventListener("touchend", mvTouchEnd, true);
      if (os !== "iOS") {
        mv.addEventListener("ar-status", mvARStatus, true);
      }
    } else {
      arLogo.setAttribute("src", "images/StanleyCupAR_QR Code.png");
      startAR.classList.remove("start-ar");
      startAR.classList.add("start-qr");
      mv.addEventListener("mousedown", mvMouseDown, true);
      mv.addEventListener("mousemove", mvMouseMove, true);
      mv.addEventListener("mouseup", mvMouseUp, true);
      mv.addEventListener("wheel", mvWheel, true);
    }
    startAR.classList.remove("ar-hide");
    arGestures.classList.remove("ar-hide");
    arGestures.classList.add("ar-gestures-show");
    changeImage(gestureImage.pan, gestureAnimation.pan);
    areListenersAdded = true;
  }
}

// INTERACTION SECTION
function currentTeam() {
  arMenu.classList.remove("ar-hide");
  for (let i = 0; i < rows.length; i++) {
    if (panY < heights[i] && panY > heights[i + 1]) {
      for (let j = 0; j < rows[i].length; j++) {
        if (rotY <= rows[i][j].left && rotY >= rows[i][j].right) {
          pointerAR.setAttribute("src", `${rows[i][j].getTeamLogoPath}`);
          teamLogoAR.setAttribute("src", `${rows[i][j].getTeamLogoPath}`);
          seasonAR.textContent = rows[i][j].getSeason;
          nameCityAR.textContent = rows[i][j].teamCity;
          nameTeamAR.textContent = rows[i][j].teamName;
          currentLink = rows[i][j].getRecordLink;
        }
        if (i === 7) {
          // FIX 75/76 Can from zero issue
          if (rotY < 7.4 && rotY >= 0) {
            pointerAR.setAttribute("src", `${rows[i][10].getTeamLogoPath}`);
            teamLogoAR.setAttribute("src", `${rows[i][10].getTeamLogoPath}`);
            seasonAR.textContent = rows[i][10].getSeason;
            nameCityAR.textContent = rows[i][10].teamCity;
            nameTeamAR.textContent = rows[i][10].teamName;
            currentLink = rows[i][10].getRecordLink;
          }
        }
        if (i === 3) {
          if (rotY < 314 && rotY >= 143) {
            pointerAR.setAttribute("src", `${bos1928.getTeamLogoPath}`);
            teamLogoAR.setAttribute("src", `${bos1928.getTeamLogoPath}`);
            seasonAR.textContent = bos1928.getSeason;
            nameCityAR.textContent = bos1928.teamCity;
            nameTeamAR.textContent = bos1928.teamName;
            currentLink = bos1928.getRecordLink;
          }
        }
        if (i === 0) {
          currentLink = "https://records.nhl.com/awards/trophies/stanley-cup";
          seasonAR.textContent = "Stanley Cup History";
          nameCityAR.textContent = "";
          nameTeamAR.textContent = "Stanley Cup History";
        } else if (i === 10) {
          emptyTeamSpace(edges[i][12], edges[i][0]);
        } else if (i === 11) {
          emptyTeamSpace(edges[i][4], 0);
          emptyTeamSpace(360, edges[i][0]);
        }
      }
    }
  }
}

function emptyTeamSpace(start, end) {
  if (rotY <= start && rotY >= end) {
    pointerAR.setAttribute("src", `images/CUP_1892_dark.svg`);
    teamLogoAR.setAttribute("src", ``);
    seasonAR.textContent = "";
    nameCityAR.textContent = "";
    nameTeamAR.textContent = "";
    currentLink = "#";
    arMenu.classList.add("ar-hide");
  }
}

function startMoving(y, x) {
  inputY = y;
  inputX = x;
  isDownTouch = true;
}
function moving(y, x) {
  if (isDownTouch === false) return;
  panY = panYStart + (y - inputY) / 1000;
  if (panY > 0.9) panY = 0.9;
  if (panY < 0) panY = 0;
  rotY = (rotYStart + (x - inputX) / 5 + 360) % 360;
  mv.cameraTarget = `0m ${panY}m 0m`;
  mv.cameraOrbit = `${-rotY}deg 90deg ${zoomPercent * 100}%`;
  currentTeam();

  // arDebugging();
}
function endMoving() {
  if (canGesture && currentGesture === "pan") {
    gestures("pan");
  } else if (canGesture && currentGesture === "rotate") {
    gestures("rotate");
  }
  if (rotY) {
    rotYStart = rotY;
  }
  if (panY) {
    panYStart = panY;
  }
  isDownTouch = false;
}
function startZooming(x0, x1) {
  startZoom = Math.abs(x0 - x1);
}
function zooming(x0, x1) {
  gestures("zoom");
  currentZoom = Math.abs(x0 - x1);
  distance = Math.abs(startZoom - currentZoom);
  pointerAR.style.opacity = 0.5 + zoomPercent;
  pointerAR.style.width = `${5 + 35 * zoomPercent}px`;
  pointerAR.style.height = `${5 + 35 * zoomPercent}px`;
  if (currentZoom > startZoom && zoomPercent > 0)
    zoomPercent -= distance / zoomSpeedReduction;
  if (currentZoom < startZoom && zoomPercent < 1.0)
    zoomPercent += distance / zoomSpeedReduction;

  mv.cameraOrbit = `${-rotY}deg 90deg ${zoomPercent * 100}%`;
}
